# Quizku
[![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/master/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/master)
[![coverage report](https://gitlab.com/tk-adprog/quizku/badges/master/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/master)

# Quizku-Quiz
[![pipeline status](https://gitlab.com/tk-adprog/quizku-quiz/badges/master/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/master)
[![coverage report](https://gitlab.com/tk-adprog/quizku-quiz/badges/master/coverage.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/master)

# Quizku-Report
[![pipeline status](https://gitlab.com/tk-adprog/quizku-report/badges/master/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/master)
[![coverage report](https://gitlab.com/tk-adprog/quizku-report/badges/master/coverage.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/master)

## Anggota
- ### Hendrico Kristiawan 1906350912  
  #### Main
  [![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/createQuiz/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/createQuiz)
  [![coverage report](https://gitlab.com/tk-adprog/quizku/badges/createQuiz/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/createQuiz)
  #### Microservice
  [![pipeline status](https://gitlab.com/tk-adprog/quizku-quiz/badges/createQuiz/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/createQuiz)
  [![coverage report](https://gitlab.com/tk-adprog/quizku-quiz/badges/createQuiz/coverage.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/createQuiz)
  <br><br>
- ### Hilmy Akbar Nugraha 1906293101 
  #### Main
  [![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/summaryQuiz/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/summaryQuiz)
  [![coverage report](https://gitlab.com/tk-adprog/quizku/badges/summaryQuiz/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/summaryQuiz)
  ### Microservice
  [![pipeline status](https://gitlab.com/tk-adprog/quizku-report/badges/summaryQuiz/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/summaryQuiz)
  [![coverage report](https://gitlab.com/tk-adprog/quizku-report/badges/summaryQuiz/coverage.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/summaryQuiz)
  <br><br>
- ### Ian Andersen Ng 1906400280  
  #### Main
  [![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/SearchAndLogin/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/SearchAndLogin)
  [![coverage report](https://gitlab.com/tk-adprog/quizku/badges/SearchAndLogin/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/SearchAndLogin)
  #### Microservice
  [![pipeline status](https://gitlab.com/tk-adprog/quizku-quiz/badges/SearchAndLogin/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/SearchAndLogin)
  [![coverage report](https://gitlab.com/tk-adprog/quizku-quiz/badges/SearchAndLogin/coverage.svg)](https://gitlab.com/tk-adprog/quizku-quiz/-/commits/SearchAndLogin)
  <br><br>
- ### Muhammad Ravi Shulthan Habibi 1906351000  
  #### Main
  [![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/jawabSoal/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/jawabSoal)
  [![coverage report](https://gitlab.com/tk-adprog/quizku/badges/jawabSoal/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/jawabSoal)
  ### Microservice
  [![pipeline status](https://gitlab.com/tk-adprog/quizku-report/badges/jawabSoal/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/jawabSoal)
  [![coverage report](https://gitlab.com/tk-adprog/quizku-report/badges/jawabSoal/coverage.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/jawabSoal)
  <br><br>
- ### Putu Wigunarta 1906307145  
  #### Main
  [![pipeline status](https://gitlab.com/tk-adprog/quizku/badges/quizProfile/pipeline.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/quizProfile)
  [![coverage report](https://gitlab.com/tk-adprog/quizku/badges/quizProfile/coverage.svg)](https://gitlab.com/tk-adprog/quizku/-/commits/quizProfile)
  ### Microservice
  [![pipeline status](https://gitlab.com/tk-adprog/quizku-report/badges/quizProfile/pipeline.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/quizProfile)
  [![coverage report](https://gitlab.com/tk-adprog/quizku-report/badges/quizProfile/coverage.svg)](https://gitlab.com/tk-adprog/quizku-report/-/commits/quizProfile)
  <br><br>
  
## Explanation about the project in the slides
[Slide](https://docs.google.com/presentation/d/1qZnduMrxsRejXncD7myWUWoP6UqUP-EGM6tYG7uOZ5U/edit?usp=sharing)  
[Wireframe](https://www.figma.com/file/YIa5zRynl53cTE6gUft2J8/ADPROG-A9?node-id=0%3A1)

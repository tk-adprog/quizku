package com.tk.quizku.app.service;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.UserQuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserQuizServiceImpl implements UserQuizService {

    @Autowired
    UserQuizRepository userQuizRepository;

    @Override
    public UserQuiz registerUser(UserQuiz user) {
        UserQuiz oldUser = userQuizRepository.findByUsername(user.getUsername());
        if (oldUser != null){
            return null;
        }
        userQuizRepository.save(user);
        return user;
    }

    @Override
    public UserQuiz getUserByUsername(String username){
        UserQuiz user = userQuizRepository.findByUsername(username);
        return user;
    }

    @Override
    public Iterable<UserQuiz> getAllUser(){
        return userQuizRepository.findAll();
    }

    @Override
    public void deleteAll(){
        userQuizRepository.deleteAll();
    }

}

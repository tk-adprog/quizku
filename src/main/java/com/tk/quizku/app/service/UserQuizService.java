package com.tk.quizku.app.service;

import com.tk.quizku.app.model.UserQuiz;

public interface UserQuizService {

    UserQuiz registerUser(UserQuiz user);

    UserQuiz getUserByUsername(String username);

    Iterable<UserQuiz> getAllUser();

    void deleteAll();
}

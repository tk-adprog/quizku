package com.tk.quizku.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(path = "/profile")
public class QuizProfileController {

    @GetMapping("/{id}")
    public String getQuizProfile(Model model, @PathVariable(value = "id")int idQuiz){
        model.addAttribute("idQuiz",idQuiz);
        return "quizProfile";
    }

}

package com.tk.quizku.app.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class SearchController {

    // digunakan untuk show landing page url yang berkaitan
    @GetMapping("")
    public String getLandingPage(Model model) {
        return "landingPage";
    }

}
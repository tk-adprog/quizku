package com.tk.quizku.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/jawab")
public class JawabController {
    
    @GetMapping(path = "/{quizId}")
    public String getJawabForm(Model model, @PathVariable(value = "quizId") int quizId) {
        model.addAttribute("idQuiz",quizId);
        return "jawabForm";
    }
}


package com.tk.quizku.app.controller;



import com.tk.quizku.app.model.UserQuiz;

import com.tk.quizku.app.service.UserQuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    private UserQuizService userService;

    // digunakan untuk show landing page url yang berkaitan
    @GetMapping("")
    public String getSignUpPage(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("Authorization");
        return "UserCreator";
    }

    // gunakan untuk search by title of quiz yang memproduksi json dari quiz yang dicari
    @PostMapping(path = "/register", produces = {"application/json"})
    @ResponseBody

    public ResponseEntity registerUser(@RequestBody UserQuiz user) {
        UserQuiz savedUser = userService.registerUser(user);
        if (savedUser == null){
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        return ResponseEntity.ok(savedUser);
    }

}
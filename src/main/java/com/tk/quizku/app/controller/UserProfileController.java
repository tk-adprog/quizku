package com.tk.quizku.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Map;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(path = "/userProfile")
public class UserProfileController {

    @Autowired
    JwtTokenUtil jwtTokenUtil;


    @GetMapping("")
    public String getQuizProfile(Model model){
        return "userProfile";
    }

}

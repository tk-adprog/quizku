package com.tk.quizku.app.controller;


import com.tk.quizku.app.model.UserQuiz;

import com.tk.quizku.app.service.UserQuizService;
import com.tk.quizku.auth.config.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Controller
@RequestMapping("/user")
public class UserQuizController {

    @Autowired
    private UserQuizService userQuizService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping(path = "", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<UserQuiz> getUser(@RequestBody Map<String, Object> json) {
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        UserQuiz user = userQuizService.getUserByUsername(username);
        return ResponseEntity.ok(user);
    }

    @GetMapping(path = "/{username}", produces = {"application/json"})
    @CrossOrigin
    @ResponseBody
    public ResponseEntity<UserQuiz> getUser( @PathVariable(value ="username") String  username ) {
        UserQuiz user = userQuizService.getUserByUsername(username);
        return ResponseEntity.ok(user);
    }

    @GetMapping(path = "/all", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<UserQuiz>> getAllUser() {
        return ResponseEntity.ok(userQuizService.getAllUser());
    }

    @PostMapping(value = "/getUsername", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<String> getUsername(@RequestBody Map<String, Object> json) throws Exception{
        String token = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(token);
        System.out.println(username);
        return ResponseEntity.ok(username);
    }

    @GetMapping(path = "/self/destruct", produces = {"application/json"})
    @CrossOrigin
    public String deleteAll() {
        userQuizService.deleteAll();
        return "redirect:/user/all";
    }

}

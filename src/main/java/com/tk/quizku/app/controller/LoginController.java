package com.tk.quizku.app.controller;

import com.tk.quizku.app.service.UserQuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private UserQuizService userService;

    // digunakan untuk show landing page url yang berkaitan
    @GetMapping("")
    public String getSignUpPage(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("Authorization");
        return "login";
    }

//    // gunakan untuk search by title of quiz yang memproduksi json dari quiz yang dicari
//    @PostMapping(path = "/check", produces = {"application/json"})
//    @ResponseBody
//    public ResponseEntity registerUser(@RequestBody UserQuiz user) {
//        return ResponseEntity.ok(userService.registerUser(user));
//    }

}
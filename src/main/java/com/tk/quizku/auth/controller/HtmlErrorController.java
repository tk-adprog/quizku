package com.tk.quizku.auth.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class HtmlErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            System.out.println("statusCode = " + statusCode);
            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                return "errorPageNotFound";
            }
            else if(statusCode == HttpStatus.UNAUTHORIZED.value()) {
                return "errorUnauthorized";
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}

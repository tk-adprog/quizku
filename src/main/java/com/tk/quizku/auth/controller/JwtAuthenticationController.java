package com.tk.quizku.auth.controller;

import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.model.JwtRequest;
import com.tk.quizku.auth.model.JwtResponse;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@CrossOrigin
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    // saat mendapat request get username dan password
    // dengan userDetailsService dapatkan user details nya
    // generate token dari user details yang didapatkan
    // return suatu jwt
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest, HttpServletRequest request) throws Exception {

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        // bikin session lalu set authorization attribute menjadi bearer dan token
        HttpSession session = request.getSession();
        session.setAttribute("Authorization", "Bearer " + token);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    // authenticate suatu user , jika gagal send exception
    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    // remove attribute biar logout
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void deleteAuthenticationToken(HttpServletRequest request){

        HttpSession session = request.getSession();
        session.removeAttribute("Authorization");
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> RefreshToken(@RequestBody Map<String, Object> json, HttpServletRequest request) throws Exception {
        String jwtToken = json.get("token").toString();
        String username = jwtTokenUtil.getUsernameFromToken(jwtToken);
        UserDetails user = userDetailsService.loadUserByUsername(username);
        String newToken = jwtTokenUtil.generateToken(user);
        HttpSession session = request.getSession();
        session.setAttribute("Authorization", "Bearer " + newToken);
        return ResponseEntity.ok(newToken);
    }
}


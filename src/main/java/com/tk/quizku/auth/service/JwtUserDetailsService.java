package com.tk.quizku.auth.service;

import com.tk.quizku.app.model.UserQuiz;
import org.springframework.security.core.userdetails.User;
import com.tk.quizku.app.repository.UserQuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class  JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserQuizRepository userQuizRepository;

    @Autowired
    private PasswordEncoder bcryptEncoder;
    // mencari user dari user repository
    // lalu masukkan username dan password dari yang bersangkutan ke dalam user details
    // $2a$10$m0IlnWw8I5P39bYv4Gmdqu8L39WJJQvHnQeEmQTDAOmYK4R3Nkv.O

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserQuiz userku = userQuizRepository.findByUsername(username);
        if (userku == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(userku.getUsername(), bcryptEncoder.encode(userku.getPassword()),
                new ArrayList<>());
    }

}
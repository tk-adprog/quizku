package com.tk.quizku.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    // encode password
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    // returns suatu authentication manager
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    // path yang mana yang diperbolehkan masuk tanpa autentikasi dan dengan autentikasi
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity.csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/authenticate").permitAll()
                .antMatchers(HttpMethod.GET,"/authenticate").permitAll()
                .antMatchers(HttpMethod.POST,"/").permitAll()
                .antMatchers(HttpMethod.POST,"/signup").permitAll()
                .antMatchers(HttpMethod.GET, "/signup").permitAll()
                .antMatchers(HttpMethod.POST,"/signup/register").permitAll()
                .antMatchers(HttpMethod.GET, "/signup/register").permitAll()
                .antMatchers(HttpMethod.GET, "/login").permitAll()
                .antMatchers(HttpMethod.POST, "/refresh").permitAll()
                .antMatchers(HttpMethod.GET, "/user/**").permitAll()
                .antMatchers(HttpMethod.GET, "/user/all").permitAll()
                .antMatchers(HttpMethod.GET, "/user/self/destruct").permitAll()
                // allow js to load
                .antMatchers(HttpMethod.GET, "/js/UserCreator.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/quizProfile.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/quizProfile.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/userProfile.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/jawabForm.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/jawabForm.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/userProfile.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/quizForm.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/quizForm.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/login.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/login.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/search.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/search.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/summaryQuiz.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/summaryQuiz.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/refresh.js").permitAll()
                .antMatchers(HttpMethod.GET, "/js/navBar.js").permitAll()
                .antMatchers(HttpMethod.POST, "/js/navBar.js").permitAll()
                .antMatchers(HttpMethod.GET, "/css/login.css").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/login.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/logo.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/register.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/wavy.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/quizkufooter.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/img/Putu.png").permitAll()
                .antMatchers(HttpMethod.GET, "/img/Ravi.png").permitAll()
                .antMatchers(HttpMethod.GET, "/img/Ian.png").permitAll()
                .antMatchers(HttpMethod.GET, "/img/Hilmy.png").permitAll()
                .antMatchers(HttpMethod.GET, "/img/Hendrico.png").permitAll()
                .antMatchers(HttpMethod.GET, "/webjars/popper.js/1.16.0/popper.min.js").permitAll()
                .antMatchers(HttpMethod.GET, "/webjars/bootstrap/4.4.1/js/bootstrap.min.js").permitAll()
                .antMatchers(HttpMethod.GET, "/webjars/jquery/3.4.1/jquery.min.js").permitAll()
                .antMatchers(HttpMethod.GET, "/webjars/bootstrap/4.4.1/css/bootstrap.min.css").permitAll()
                .antMatchers(HttpMethod.GET, "/css/errorStyles.css").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/shape1.svg").permitAll()
                .antMatchers(HttpMethod.GET, "/svg/shape2.svg").permitAll()
                // all other requests need to be authenticated
                .anyRequest().authenticated().and().
                // make sure we use stateless session; session won't be used to
                // store user's state.
                        exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Add a filter to validate the tokens with every request
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
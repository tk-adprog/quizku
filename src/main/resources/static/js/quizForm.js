$(document).ready(function () {

    $(document).on('click', "#finish", function () {
        var json = {}
        var title = $("#form-quiz").find("#title").val();
        if (title.length == 0) {
            alert('Title is empty!')
            return false;
        }
        var imageUrl = $("#form-quiz").find("#imageUrl").val();
        if (imageUrl.length == 0) {
            alert('Image url is empty!')
            return false;
        } else if (imageUrl.length > 255){
            alert('Url to long!');
        }
        var duration = $("#form-quiz").find("#duration").val();
        if (duration < 10 || duration > 120) {
            alert('Duration must between 10 and 120 minutes!')
            return false;
        }
        if (duration.length == 0) {
            alert('Duration is empty!')
            return false;
        }
        var description = $("#form-quiz").find("#desc").val();
        if (description.length == 0) {
            alert('Description is empty!')
            return false;
        }
        var tag = $("#form-quiz").find("#tag").val();
        if (tag.length == 0) {
            alert('Tag is empty!')
            return false;
        }
        json["title"] = title;
        json["description"] = description;
        json["duration"] = duration*60;
        json["tag"] = tag;
        json["imageUrl"] = imageUrl;

        var questionList = []
        var questionElement = $("#form-question").find(".type");
        if (questionElement.length == 0) {
            alert("No Question!");
            return false;
        }
        var isLoopSuccess = true;
        // Loop every form
        jQuery.each(questionElement, function (i, iVal) {
            if (isLoopSuccess == false) {
                return false;
            }
            questionJson = {}

            var type = $(iVal).attr("value")
            questionJson["type"] = type;
            var eachQuestionElement = $(iVal).children();
            var questionInputElement = $(eachQuestionElement[1]).children()
            var choiceInputElement = $(eachQuestionElement[3]).children();
            // Get question string
            var question = questionInputElement.val();
            if (question.length == 0) {
                alert('Question is empty!')
                isLoopSuccess = false
            }
            questionJson["question"] = question;
            var optionArray = [];

            // Get choice string
            jQuery.each(choiceInputElement, function (j, jVal) {
                if (isLoopSuccess == false) {
                    return false;
                }
                if ($(jVal).val().length == 0) {
                    alert('Answer is empty!')
                    isLoopSuccess = false;
                }
                optionArray.push($(jVal).val());
            })
            questionJson["options"] = optionArray;
            questionList.push(questionJson);
        });
        if (!isLoopSuccess) {
            return false;
        }
        json["questions"] = questionList;
        json["token"] = sessionStorage.getItem("token");
        json = JSON.stringify(json)
        $.ajax({
            type: 'POST',
            //url: "http://localhost:8082/quiz/register",
            url: "https://quizku-quiz.herokuapp.com/quiz/register",
            crossDomain : true,
            data: json,
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr){
                xhr.setRequestHeader('Authorization', "Bearer " + sessionStorage.getItem("token"));
            },
            success: function (data) {
                window.location = '/profile/' + data.idQuiz;
            },
            fail: function () {
                alert('ERROR!');
            }
        });
    });

    $(document).on('click', "#next", function () {
        var quizDiv = $(document).find("#quiz-div")
        var questionDiv = $(document).find("#question-div")
        $(quizDiv).addClass("d-none")
        $(questionDiv).removeClass("d-none")
    });

    $(document).on('click', "#back", function () {
        var quizDiv = $(document).find("#quiz-div")
        var questionDiv = $(document).find("#question-div")
        $(questionDiv).addClass("d-none")
        $(quizDiv).removeClass("d-none")
    });

    $(".choiceButton").click(function () {
        $("#listQuestion").append($(`
                            <li>
                                <div class="form-group type" name="type" value="Choice">
                                    <h2 id="titlenya" class="float-left">Question</h2>
                                    <div>
                                        <textarea type="text" class="form-control question" name="question" placeholder="Type the Question" maxlength="255" style="height: 8em; background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); color: white; border-radius: 15px;"></textarea>
                                    </div>
                                    <h2 id="titlenya" class="float-left">Answer</h2>
                                    <div class="answer">
                                        <input type="text" class="form-control" name="a" id="a" placeholder="True Option" maxlength="255" style="background-color: #B0D9CD">
                                        <input type="text" class="form-control" name="b" id="b" placeholder="False Option" maxlength="255" style="background-color: rgba(217, 176, 176, 1);">
                                        <input type="text" class="form-control" name="c" id="c" placeholder="False Option" maxlength="255" style="background-color: rgba(217, 176, 176, 1);">
                                        <input type="text" class="form-control" name="d" id="d" placeholder="False Option" maxlength="255" style="background-color: rgba(217, 176, 176, 1);">
                                    </div>
                                </div>
                                <div class="deleteButton ">
                                    <i class="far fa-trash-alt"></i>
                                </div>
                            </li>
                            <br>
        `));
    });

    $(".essayButton").click(function () {
        $("#listQuestion").append($(`
                            <li>
                                <div class="form-group type" name="type" value="Essay">
                                    <h2 id="titlenya" class="float-left">Question</h2>
                                    <div>
                                        <textarea type="text" class="form-control question" name="question" placeholder="Type the question" maxlength="255" style="height: 8em; background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); color: white; border-radius: 15px;"></textarea>
                                    </div>
                                    <h2 id="titlenya" class="float-left">Answer</h2>
                                    <div class="question">
                                        <textarea type="text" class="form-control" name="answer" id="answer" placeholder="Answer" maxlength="255" style="height: 8em; border-radius: 15px;"></textarea>
                                    </div>
                                </div>
                                <div class="deleteButton">
                                    <i class="far fa-trash-alt"></i>
                                </div>
                            </li>
                            <br>

        `));
    });

    $(document).on('click', ".deleteButton", function () {
        $(this).parent().remove();
    });

});
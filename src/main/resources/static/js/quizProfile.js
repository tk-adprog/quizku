$(document).ready(function(){
    var json = {}
    var quizId = $('#idQuizInput').val();

    json['idQuiz'] = quizId;
    json['token'] = sessionStorage.getItem("token");
    json = JSON.stringify(json);

    $.ajax({
        type: 'POST',
        url: 'https://quizku-report.herokuapp.com/profile/' + quizId,
        //url: 'http://localhost:8081/profile/' + quizId,
        crossDomain : true,
        contentType: "application/json; charset=utf-8",
        data: json,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            let fotoQuiz = `<object data="${data[6]}" type="image/png" id="imageCard">
                            <img id="imageCard" src="https://prod-discovery.edx-cdn.org/media/course/image/825420a7-fa09-4c94-8deb-1e4decb916f4-56f44cd9d8e7.small.jpeg" alt="Stack Overflow logo and icons and such">
                            </object>`;
            // let fotoQuiz = `<img
            //     id="imageCard"
            //     className="card-img-top"
            //     src="${data[6]}"
            //     alt="Card image cap" style="border-radius: 1.5vh 1.5vh 0vh 0vh;"
            // />`;
            var duration = data[4]/60;
            let dataQuiz = '';
            dataQuiz+= `<h5 class="card-title">Tittle : ${data[2]}</h5>  ` +
                `<h5 class="card-title">Questions : ${data[3]}</h5> ` +
                `<h5 class="card-title">Duration: ${duration} m</h5> ` +
                `<h5 class="card-text">${data[5]}</h5> `
                if(data[1] == 0){
                    dataQuiz+= `<a href="/jawab/${quizId}" class="btn btn-primary d-flex justify-content-center hijauGradient" style="border-radius: 1vh">Attempt Now</a>`
                }else{
                    dataQuiz+= `<a href="/summary/${quizId}" class="btn btn-primary d-flex justify-content-center hijauGradient" style="border-radius: 1vh">See Review</a>`
                }
            $('#dataQuiz').html(dataQuiz);
            $('#imageQuiz').html(fotoQuiz);
        }
    })

    $.ajax({
        type:'GET',
        url:'https://quizku-report.herokuapp.com/profile/leaderboard/' + quizId,
        //url: 'http://localhost:8081/profile/leaderboard/' + quizId,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            let result = '';
            if(data.length > 0){
                result += '<div class="row leaderboardTitle"> ' +
                    '<div class="col-sm">No</div> ' +
                    '<div class="col-sm text-center">Nama Peserta</div> ' +
                    '<div class="col-sm text-center">Score</div> ' +
                    '</div>'
                for(let i = 0; i < data.length; i++){
                    counter = i+1
                    result += `<div class="row hijauGradient bgJudul leaderboardList"> ` +
                        `<div class="col-sm">${counter}</div> ` +
                        `<div class="col-sm text-center">${data[i].quizReportId.owner}</div>  ` +
                        `<div class="col-sm text-center">${data[i].grade}</div> ` +
                        `</div>`;

                }
            }else{
             result += '<div class="d-flex justify-content-center">There is no data for this Quiz</div>'
            }
            $('#resultLeaderboard').html(result);
        }
    })

    $.ajax({
        type:"POST",
        url:'https://quizku-report.herokuapp.com/profile/statistics/' + quizId,
        //url: 'http://localhost:8081/profile/statistics/' + quizId,
        crossDomain : true,
        contentType: "application/json; charset=utf-8",
        data: json,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            console.log(data[2]);
            let detik = data[2]%60;
            let menit = (data[2]-detik)/60;
            console.log(menit);
            console.log(detik);
            $('#avgScore').html(data[0]);
            $('#avgDuration').html(`${menit}m${detik}s`);
            $('#ttlAttempt').html(data[1]);
        }
    })

})


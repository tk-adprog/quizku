
    var json = {}
    var tokenString = sessionStorage.getItem("token")
    if (tokenString.length == 0){
        tokenString = "";
    }
    json["token"] = tokenString;
    json = JSON.stringify(json)
    $.ajax({
        type: 'POST',
        url: '/refresh',
        contentType: "application/json; charset=utf-8",
        data: json,
        success: function (data) {
            sessionStorage.setItem("token",data);
            sessionStorage.setItem("Authorization", 'Bearer ' + data);
            if (document.title == "Unauthorized"){
                location.reload();

            }

        },
        error:function (){
            window.location="/login";
        }
    })

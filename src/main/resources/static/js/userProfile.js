$(document).ready(function () {
    var json={};
    json['token'] = sessionStorage.getItem("token");
    json = JSON.stringify(json);

    $.ajax({
        type: 'POST',
        crossDomain : true,
        url : '/user',
        contentType: "application/json; charset=utf-8",
        dataType : 'json',
        data: json,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            let fotoProfile = `<img src="${data.imageUrl}" class="rounded-circle profile-photos" id="profileImage" onerror="if (this.src != '/img/profile photo.png') this.src = '/img/profile photo.png';">`;
            $('#imageProfile').html(fotoProfile)
            $('#usernameContent').append(data.username);
            $('#fullName').html(data.fullname);
            $('#email').html(data.email);
        }
    })

    $.ajax({
        type: 'POST',
        crossDomain : true,
        url : 'https://quizku-quiz.herokuapp.com/quiz/getQuiz',
        //url : 'http://localhost:8082/quiz/getQuiz',
        contentType: "application/json; charset=utf-8",
        dataType : 'json',
        data: json,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            let result = '';
            if(data.length > 0){
                for(let i = 0; i < data.length; i++){
                    counter = i+1
                    result += `<div class="col-sm d-flex justify-content-center text-center"> 
                                <div class="card mt-5 mb-5" style="width: 18rem;">
                                <img src="${data[i].imageUrl}" class="card-img-top"
                                onerror="this.src = 'https://prod-discovery.edx-cdn.org/media/course/image/825420a7-fa09-4c94-8deb-1e4decb916f4-56f44cd9d8e7.small.jpeg';">
                                <div class="card-body">
                                    <h5 class="card-title tulisanHijau">${data[i].title}</h5>
                                    <p class="card-text tulisanHijau">${data[i].description}</p>
                                    <a type="submit" class="btn btn-primary hijauGradient" id="likesBtn" href="/profile/${data[i].idQuiz}">View Page</a>
                                </div>
                            </div>
                        </div>`;

                }
            }else{
                result += '<div class="d-flex justify-content-center">There is no Quiz created</div>'
            }
            $('#resultQuizCreated').html(result);
        }
    })

    $.ajax({
        type: 'POST',
        crossDomain : true,
        //url : "http://localhost:8081/report/getQuizAnswered",
        url : 'https://quizku-report.herokuapp.com/report/getQuizAnswered',
        contentType: "application/json; charset=utf-8",
        dataType : 'json',
        data: json,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            let result = '';
            if(data.length > 0){
                for(let i = 0; i < data.length; i++){
                    counter = i+1
                    result += `<div class="col-sm d-flex justify-content-center text-center">
                                <div class="card mt-5 mb-5" style="width: 18rem;">
                                <img src="${data[i].imageUrl}" class="card-img-top"
                                onerror="this.src = 'https://prod-discovery.edx-cdn.org/media/course/image/825420a7-fa09-4c94-8deb-1e4decb916f4-56f44cd9d8e7.small.jpeg';">
                                <div class="card-body">
                                    <h5 class="card-title tulisanHijau">${data[i].title}</h5>
                                    <p class="card-text tulisanHijau">${data[i].description}</p>
                                    <a type="submit" class="btn btn-primary hijauGradient" id="likesBtn" href="/profile/${data[i].idQuiz}">View Page</a>
                                </div>
                            </div>
                        </div>`;

                }
            }else{
                result += '<div class="d-flex justify-content-center">There is no data for this Quiz</div>'
            }
            $('#resultQuizAnswered').html(result);
        }
    })
})
const inputs = document.querySelectorAll(".input");
sessionStorage.clear();
function addcl(){
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function remcl(){
    let parent = this.parentNode.parentNode;
    if(this.value == ""){
        parent.classList.remove("focus");
    }
}

inputs.forEach(input => {
    input.addEventListener("focus", addcl);
    input.addEventListener("blur", remcl);
});

$(document).ready(function () {
    $("#create").click(function () {
        var json = {}
        var username = $("#username").val();
        if (username.length == 0) {
            alert('Username is empty!')
            return false;
        }

        var fullname = $("#fullname").val();
        if (fullname.length == 0) {
            alert('Fullname is empty!')
            return false;
        }

        var email = $("#email").val();
        if (email.length == 0) {
            alert('Email is empty!')
            return false;
        } else if (!ValidateEmail(email)){
            alert('Invalid email address!');
        }

        var birthdate = $("#birthdate").val();
        if (birthdate.length < 10) {
            alert('Invalid date format!')
            return false;
        }

        var imageUrl = $("#imageUrl").val();
        if (imageUrl.length == 0) {
            alert('Image url is empty!')
            return false;
        } else if (imageUrl.length > 255){
            alert('Url to long!');
        }

        var password = $("#password").val();
        if (password.length == 0) {
            alert('Password is empty!')
            return false;
        }


        json["username"] = username;
        json["fullname"] = fullname;
        json["email"] = email;
        json["birthdate"] = birthdate;
        json["imageUrl"] = imageUrl;
        json['password'] = password;
        json = JSON.stringify(json);
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: "/signup/register",
            data: json,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                alert('Register succeed. Please login!')
                console.log("registered here")
            },
            error: function (){
                alert('Username already used!');
            },
            fail: function () {
                alert('ERROR!');
            }
        });

        // ke quizku quiz
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: "https://quizku-quiz.herokuapp.com/signup/register",
            //url: "http://localhost:8082/signup/register",
            data: json,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log("register to quiz")
            },
            error: function (){

            },
            fail: function () {
                alert('ERROR!');
            }
        });

        // ke quizku report
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            url: "https://quizku-report.herokuapp.com/signup/register",
            //url: "http://localhost:8081/signup/register",
            data: json,
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                console.log("register to report")
                window.location = '/login'
            },
            error: function (){
            },
            fail: function () {
                alert('ERROR!');
            }
        });
    });

    function ValidateEmail(input) {
        const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (input.match(validRegex)) {
            return true;

        } else {
            return false;
        }

    }
})

$("#searchBar").on('keypress',function(e) {
    if(e.which == 13) {
        searchAsync("");
    }
});

// implemented a small function
// hanya satu tujuan mngubah isi dari param search ke dalam json untuk digunakan di tempat lain

$("#searchButton").click(function () {
    searchAsync("");
})

$("#logout").click(function (){
    $.ajax({
        type: 'POST',
        url: "/logout"
    }).done(function (response){
        sessionStorage.removeItem("Authorization")
        sessionStorage.removeItem("token")
        window.location.href = "/login";
    })
})

// search with tag
$("#tagButton").click(function () {
    searchAsync("tag/");
})

// gunakan ajax untuk load json secara async dan lempar ke front end

function searchAsync(tag) {
    var paramSearch = document.getElementById("searchBar").value

    $.ajax({
        type: 'GET',
        crossDomain : true,
        url: "https://quizku-quiz.herokuapp.com/"+tag+paramSearch,
        //url: "http://localhost:8082/"+tag+paramSearch,
        contentType: "application/json; charset=utf-8",
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data){
            var i = 0, dataSize = data.length, hasil = ""
            if(dataSize > 0){
                for(i; i < dataSize; i++){
                    var idRedirect = data[i].idQuiz
                    hasil +=  `            <div class="col-md-4">` +
                        `                <div class="card">` +
                        `                    <div class="img1 img-thumbnail"><img src= '${data[i].imageUrl}' onerror="this.src ='https://prod-discovery.edx-cdn.org/media/course/image/825420a7-fa09-4c94-8deb-1e4decb916f4-56f44cd9d8e7.small.jpeg\';"></div>` +
                        `                    <div class="main-text">` +
                        `                        <h2>${data[i].title}</h2>` +
                        `                        <p>${data[i].description}</p>` +
                        `                    </div>` +
                        `                </div>` +
                        `                <br>` +
                        `<a href="profile/${idRedirect}" class="btn btn-primary d-flex justify-content-center hijauGradient" style="border-radius: 1vh">Visit profile</a>`+
                        `            </div>`
                }
            }else{
                hasil = `<div class="text-center resultText">The Data You Search Is Not Exsist</div>`
            }

            $("#output-search").empty();
            $("#output-search").append(hasil);
        },
        fail: function () {
            alert('ERROR!');
        },
        error: function (){
            console.log("error")
        }
    })



}


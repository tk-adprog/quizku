$(document).ready(function(){

    var map = {}
    map["token"] = sessionStorage.getItem("token");

    map = JSON.stringify(map);

    var json = {}
    var quizId = $('#idQuizInput').val();


    $.ajax({
        type:"POST",
        url: 'https://quizku-report.herokuapp.com/jawab/' + quizId + '/durationLeft',
        //url: 'http://localhost:8081/jawab/' + quizId + '/durationLeft',
        crossDomain : true,
        contentType: "application/json; charset=utf-8",
        data: map,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success:function(data){
            var myVar;
            var minute = data[0];
            var seconds = data[1];
            $("#timerr").html(`${minute} minutes ${seconds} seconds`);
            var countDownSeconds;
            startTime();
            function startTime(){
                if(seconds == 0){
                    minute--;
                }
                if (minute <= 0 && seconds <= 0){
                    $( "#submit" ).trigger( "click" );
                }
                myVar = setInterval(start, 1000);
                countDownSeconds = seconds;
                $("#timerr").html(`${minute} minutes ${seconds} seconds`);
            }

            function start(){
                seconds--;
                $("#timerr").html(`${minute} minutes ${seconds} seconds`);
                if(seconds == 0){
                    minute--;
                    seconds = 59;
                }
                if (minute == 0 && seconds == 0){
                    $( "#submit" ).trigger( "click" );
                }
            }
        }
    })

    $.ajax({
        type: "POST",
        url: 'https://quizku-report.herokuapp.com/jawab/' + quizId,
        //url: 'http://localhost:8081/jawab/' + quizId,
        crossDomain : true,
        contentType: "application/json; charset=utf-8",
        data: map,
        beforeSend: function (xhr){
            xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
        },
        success: function(data) {
            $("#title").append($(
                '<h1 >' + data.title + '</h1>'
            ));
            jQuery.each(data.questionList, function (i, question) {
                if (question.option.length == 0) {
                    var string = '<div class= "kelompok"> <div class="question essay">' +
                        '<p>' + question.question + '</p>' +
                        '<div class="number" name="'+ question.questionId.number +'"> </div>' +
                        '<div class="tipe" name= "essay"> </div> </div> <br>' + '<p style="font-size: 0.8em; margin-left: 1em">Type your answer !</p>' +
                        '<div class="answer"> <textarea type="text" class="form-control selectedEssay" name="answerEssay" maxlength="255" placeholder="Isi disini"></textarea> </div> </div>'
                    $("#inject1").append($(
                        string + '<br><br>'
                    ))
                }
                else {
                    var string = '<div class= "kelompok"> <div class="question pilgan" name =' + question.questionId.number +'">' +
                        '<p>' + question.question + '</p>' +
                        '<div class="number" name=" '+ question.questionId.number +'"> </div>' +
                        '<div class="tipe" name= "pilgan"> </div> </div> <br>' + '<p style="font-size: 0.8em; margin-left: 1em">Choose your answer !</p>' +
                        '<div class="answer">'
                    jQuery.each(question.option, function (j, option) {
                        string += '<div class="option" name="answer">' + option + '</div>';
                    })
                    string += '</div>'
                    $("#inject1").append($(
                        string + '<br><br>'
                    ))
                }
            })

        }
    });

    $(document).on('click', ".option", function () {
        var parent = $(this).parent();

        $(parent).children('div').each(function () {
            $(this).removeClass('selected');
        });

        $(this).addClass('selected');
    });

    $(document).on('click', "#submit", function () {
        var json = {}
        var list = []
        var kelompok = $("#inject1").find(".kelompok");

        jQuery.each(kelompok, function (k, question) {
            var valueList = {}
            var questionElement = $(question).find(".question");
            var questionStringElement = $(questionElement).find('p');
            valueList["questionString"] = $(questionStringElement).text();

            var nomorPertanyaan = $(questionElement).find(".number");
            valueList["number"] = $(nomorPertanyaan).attr('name');

            var tipePertanyaan = $(questionElement).find(".tipe");
            valueList["tipe"] = $(tipePertanyaan).attr('name');

            if ($(tipePertanyaan).attr('name') == "essay") {
                var jawaban = $(question).find(".selectedEssay");
                var jawab = $(jawaban).val();
                if (jawab.length == 0){
                    jawab = "";
                }
                valueList["answer"] = jawab;

            }
            else if ($(tipePertanyaan).attr('name') == "pilgan") {
                var jawab = $(question).find(".selected").text();
                if (jawab.length == 0){
                    jawab = "";
                }
                valueList["answer"] = jawab;
            }
            list.push(valueList);

        });


        var map = {}
        map["json"] = list;
        map["token"] = sessionStorage.getItem("token");

        map = JSON.stringify(map);
        $.ajax({
            type: 'POST',
            url: 'https://quizku-report.herokuapp.com/jawab/' + quizId + '/submit',
            //url: 'http://localhost:8081/jawab/' + quizId + '/submit',
            crossDomain : true,
            contentType: "application/json; charset=utf-8",
            data: map,
            beforeSend: function (xhr){
                xhr.setRequestHeader( "Authorization", "Bearer " + sessionStorage.getItem("token"));
            },
            success: function (data) {
                window.location = `/profile/${data.quizReportId.quizId}`
            },
            fail: function () {

                alert('ERROR!');
            }
        });
    });
});

$( document ).ready(function() {
    var json = {}
    var quizId = $('#idQuizInput').val();

    json['token'] = sessionStorage.getItem('token');
    json = JSON.stringify(json);

    $("#back").append('<a style="text-decoration: none;" href=/profile/'+ quizId +'' +
        '><i class="fas fa-chevron-left"><span style="font-weight: 500; ">  ' +
        'Back to leaderboard</span></a>' +
        '</i>')

    $.ajax({
        type : "POST",
        crossDomain : true,
        beforeSend: function(xhr){
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem("token"))
        },
        //url : "http://localhost:8081/summary/" + quizId + "/gradeNya",
        url : "https://quizku-report.herokuapp.com/summary/" + quizId + "/gradeNya",
        contentType: "application/json; charset=utf-8",
        data : json,
        success: function(data){
            $("#nilai").append('Score : ' + data)
        }
    })
    $.ajax({
        type : "POST",
        crossDomain : true,
        beforeSend: function(xhr){
            xhr.setRequestHeader("Authorization", "Bearer " + sessionStorage.getItem("token"))
        },
        //url : "http://localhost:8081/summary/" + quizId + "/questionReport",
        url : "https://quizku-report.herokuapp.com/summary/" + quizId + "/questionReport",
        contentType: "application/json; charset=utf-8",
        dataType : 'json',
        data : json,
        success: function(data){
            var jumlahSoal = data.length;
            $("#jumlahSoal").append('out of ' + jumlahSoal)
            for (i = 0; i < data.length; i++) {
                var nomor = i + 1;
                var question = data[i].questionString;
                var rightAnswer = data[i].rightAnswer;
                var isTrue = data[i].true;
                var userAnswer = data[i].userAnswer;
                if (isTrue == true){
                    $("#kotak").append(
                        '    <div class="box-header">\n' +
                        '      <h3>Question ' + nomor + '<span id = "tf" style="font-size: 0.8em; background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); color: #FFFFFF; padding-left: 1em; padding-right: 1em; border-radius: 2em; margin-left: 1em" >True </span></h3>\n' +
                        '    </div>' +
                        '<div class="question">\n' +
                        question +
                        '      <br><br><span style="font-size: 0.8em;"> Your Answer : </span><div class="answer" style="background: linear-gradient(270.17deg, #B0D9CD 29.34%, #80BFB4 138.13%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + userAnswer + '</p>\n' +
                        '      </div><br>\n' +
                        '    </div><br><br>')
                } else {
                    $("#kotak").append(
                        '    <div class="box-header">\n' +
                        '      <h3>Question ' + nomor + '<span id = "tf" style="font-size: 0.8em; background: linear-gradient(90deg, #8C1F1F 0%, #BF2424 111.76%); color: #FFFFFF; padding-left: 1em; padding-right: 1em; border-radius: 2em; margin-left: 1em" >False </span></h3>\n' +
                        '    </div>' +
                        '<div class="question">\n' +
                        question +
                        '      <br><br><span style="font-size: 0.8em;"> Your Answer : </span><div class="answer" style="background: linear-gradient(270.17deg, #D9B0B0 29.34%, #BF8080 138.13%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + userAnswer + '</p>\n' +
                        '      </div>\n' +
                        '      <span style="font-size: 0.8em;"> Right Answer : </span><div class="answer" style="background: linear-gradient(90deg, #1F8C8C 0%, #24BFA3 111.76%); border-radius: 1em;">\n' +
                        '        <p style="font-size: 1em; padding-left: 1em;">' + rightAnswer + '</p>\n' +
                        '      </div><br>\n' +
                        '    </div><br><br>')
                }
            }
        }
    })
});
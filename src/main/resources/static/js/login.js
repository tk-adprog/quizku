const inputs = document.querySelectorAll(".input");

sessionStorage.clear();

function addcl(){
    let parent = this.parentNode.parentNode;
    parent.classList.add("focus");
}

function remcl(){
    let parent = this.parentNode.parentNode;
    if(this.value == ""){
        parent.classList.remove("focus");
    }
}


inputs.forEach(input => {
    input.addEventListener("focus", addcl);
    input.addEventListener("blur", remcl);
});

$(document).ready(function () {
    $("#submit").click(function () {
        $.ajax({
            type: 'POST',
            header : { "Authorization": "Bearer " + sessionStorage.getItem("token")},
            data: JSON.stringify({username: $("#username").val(), password: $("#password").val()}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "/authenticate"
        }).done(function (response){
            sessionStorage.setItem("token",response.token);
            sessionStorage.setItem("Authorization", 'Bearer ' + sessionStorage.getItem("token"))

            window.location.href = "/";
        })

    });
});
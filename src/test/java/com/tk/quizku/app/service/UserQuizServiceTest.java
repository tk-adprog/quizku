package com.tk.quizku.app.service;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.UserQuizRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserQuizServiceTest {

    @Mock
    UserQuizRepository userQuizRepository;
    @InjectMocks
    UserQuizServiceImpl userQuizService;
    UserQuiz userQuiz;

    @BeforeEach
    public void setUp() {
        userQuiz = new UserQuiz();
        userQuiz.setUsername("username");
    }

    @Test
    public void testRegisterUser() {
        when(userQuizRepository.findByUsername(any())).thenReturn(null);
        UserQuiz created = userQuizService.registerUser(userQuiz);
        assertEquals(userQuiz.getUsername(), created.getUsername());
    }

    @Test
    public void testRegisterUserButUsernameAlreadyUsed() {
        when(userQuizRepository.findByUsername(any())).thenReturn(userQuiz);
        UserQuiz created = userQuizService.registerUser(userQuiz);
        assertEquals(null, created);
    }

    @Test
    public void testGetUserQuizByUsername() {
        when(userQuizRepository.findByUsername("username")).thenReturn(userQuiz);
        UserQuiz found = userQuizService.getUserByUsername("username");
        assertEquals(userQuiz.getUsername(), found.getUsername());
    }

    @Test
    public void testGetListUserQuiz(){
        ArrayList<UserQuiz> list = new ArrayList<>();
        list.add(userQuiz);
        when(userQuizRepository.findAll()).thenReturn(list);
        Iterable<UserQuiz> iterUser = userQuizService.getAllUser();
        assertEquals(list, iterUser);
    }

    @Test
    public void testDeleteAll(){
        userQuizService.deleteAll();
    }


}

package com.tk.quizku.app.controller;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class JawabControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private UserQuiz userQuiz;

    private UserDetails userQuizUserDetail;

    private String token;

    @BeforeEach
    public void setUp() {

    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testGetJawabForm() throws Exception {
        mvc.perform(get("/jawab/1")
                .header(HttpHeaders.AUTHORIZATION, "Bearer Token"))
                .andExpect(status().isOk());
    }

}

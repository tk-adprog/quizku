
package com.tk.quizku.app.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.service.UserQuizServiceImpl;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserQuizController.class)
public class UserQuizControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserQuizServiceImpl userService;
    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private UserQuiz userQuiz;
    private String userQuizToken;
    private UserDetails userQuizUserDetail;

    private UserQuiz user;

    @BeforeEach
    public void setUp() {
        Date tanggal =  (new Date(2001, 3, 3));

        userQuiz = new UserQuiz( "ian.andersen",
                "Ian Andersen Ng",
                "ian@gmail.com",
                tanggal,
                "raja.com",
                "password"
        );

        userQuizUserDetail = new User("ian.andersen","password",new LinkedList<>());
        userQuizToken = jwtTokenUtil.generateToken(userQuizUserDetail);
        user = new UserQuiz();
        user.setUsername("uname");
        user.setEmail("a@a.com");
        user.setFullname("fname");
        user.setImageUrl("a");
        user.setBirthDate(new Date(2001, 3, 3));
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

/*    @Test
    @WithMockUser(username = "ian.andersen")
    public void testUserControllerPostUser() throws Exception {
        when(userService.getUserByUsername(user.getUsername())).thenReturn(user);

        mvc.perform(get("/user/uname").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(user)))
                .andExpect(jsonPath("$.username").value("uname"));
    }*/

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testUserControllerGetAllUser() throws Exception {
        ArrayList<UserQuiz> list = new ArrayList<>();
        list.add(userQuiz);
        when(userService.getAllUser()).thenReturn(list);

        mvc.perform(get("/user/all").contentType(
                MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].username").value("ian.andersen"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testUserControllerGetUsername() throws Exception {
        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn("username");
        HashMap<String, String> json = new HashMap<>();
        json.put("token", "token");

        mvc.perform(post("/user/getUsername").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(json)))
                .andExpect(jsonPath("$").value("username"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testUserControllerGetUser() throws Exception {
        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn("username");
        when(userService.getUserByUsername(any())).thenReturn(userQuiz);
        HashMap<String, String> json = new HashMap<>();
        json.put("token", "token");

        mvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(json)))
                .andExpect(jsonPath("$.username").value("ian.andersen"));
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testUserControllerDeleteAll() throws Exception {
        mvc.perform(get("/user/self/destruct")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerGetUser() throws Exception {
        when(userService.getUserByUsername("ian.andersen")).thenReturn(userQuiz);
        mvc.perform(get("/user/ian.andersen")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isOk());

    }


}
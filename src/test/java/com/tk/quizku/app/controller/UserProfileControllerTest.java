package com.tk.quizku.app.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.auth.config.JwtAuthenticationEntryPoint;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserProfileController.class)
public class UserProfileControllerTest {

    private final Map<String, Object> json = new HashMap<>();
    @Autowired
    private MockMvc mvc;

    @MockBean
    private JwtUserDetailsService jwtUserDetailsService;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    private UserQuiz userQuiz;
    private String userQuizToken = "token";
    private UserDetails userQuizUserDetail;

    @BeforeEach
    public void setUp() {
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    @WithMockUser(username = "ian.andersen")
    public void testQuizControllerPage() throws Exception {
        mvc.perform(get("/userProfile")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + userQuizToken))
                .andExpect(status().isOk());
    }
}

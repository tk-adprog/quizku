package com.tk.quizku.auth.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.auth.config.JwtTokenUtil;
import com.tk.quizku.auth.model.JwtRequest;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.LinkedList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class JwtAuthenticationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private JwtTokenUtil jwtTokenUtil;

    @MockBean
    private JwtUserDetailsService userDetailsService;

    private UserQuiz userQuiz;
    private UserDetails suatuUser;
    private final String userQuiz_username = "ian";
    private final String userQuiz_pass = "password";

    @BeforeEach
    public void setUp() {
        userQuiz = new UserQuiz();
        userQuiz.setUsername(userQuiz_username);
        userQuiz.setPassword(userQuiz_pass);

        suatuUser = new User(userQuiz_username,userQuiz_pass,new LinkedList<>());
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testAuthenticateCorrectCredentialsShouldSucceed() throws Exception {
        String dummyToken = "ABCDE";

        when(userDetailsService.loadUserByUsername(userQuiz_username)).thenReturn(suatuUser);
        when(jwtTokenUtil.generateToken(suatuUser)).thenReturn(dummyToken);

        JwtRequest request = new JwtRequest(userQuiz_username, userQuiz_pass);

        mvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(request)))
                .andExpect(jsonPath("$.token").value(dummyToken));
    }

    @Test
    public void testAuthenticateWrongCredentialShouldFail() throws Exception {
        when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);

        JwtRequest request = new JwtRequest(userQuiz_username, "badpassword012");

        mvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(request)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testLogout() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        JwtAuthenticationController authenticationController = new JwtAuthenticationController();
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("Authorization", "token");
        request.setSession(session);
        authenticationController.deleteAuthenticationToken(request);
    }

    @Test
    public void testRefresh() throws Exception {
        HashMap<String, String> json = new HashMap<>();
        json.put("token", "token");
        when(jwtTokenUtil.getUsernameFromToken(any())).thenReturn("username");
        when(userDetailsService.loadUserByUsername(any())).thenReturn(suatuUser);
        when(jwtTokenUtil.generateToken(any())).thenReturn("token");
        mvc.perform(post("/refresh")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(json)))
                .andExpect(status().isOk());
    }


}

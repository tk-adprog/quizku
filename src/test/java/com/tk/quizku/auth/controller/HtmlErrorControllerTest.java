package com.tk.quizku.auth.controller;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.RequestDispatcher;

public class HtmlErrorControllerTest {

    @Mock
    MockHttpServletRequest request = new MockHttpServletRequest();;
    @InjectMocks
    HtmlErrorController htmlErrorController = new HtmlErrorController();



    @Test
    public void testGetErrorPath(){
        htmlErrorController.getErrorPath();
    }

    @Test
    public void testErrorUnauthorized(){
        request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, 401);
        htmlErrorController.handleError(request);
    }

    @Test
    public void testErrorNotFound(){
        request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, 404);
        htmlErrorController.handleError(request);
    }

    @Test
    public void testErrorOtherError(){
        request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, null);
        htmlErrorController.handleError(request);
        request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, 405);
        htmlErrorController.handleError(request);
    }
}

package com.tk.quizku.auth.service;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.app.repository.UserQuizRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JwtUserDetailsServiceTest {

    @InjectMocks
    JwtUserDetailsService jwtUserDetailsService;

    @Mock
    UserQuizRepository userQuizRepository;

    @Mock
    PasswordEncoder bcryptEncoder;

    private UserQuiz userQuiz;

    private final String wrongUsername = "jason";
    private final String userQuiz_username = "ian";
    private final String userQuiz_pass = "password";
    private final String userQuiz_pass_enc ="password";
    @BeforeEach
    public void setUp() {
        userQuiz = new UserQuiz(
                userQuiz_username,
                "Ian Andersen Ng",
                "ian@mail.com",
                null,
                "blabla.jpg",
                userQuiz_pass
        );
        System.out.println(userQuiz);

    }

    @Test
    public void testLoadUserByUsernameIsSuccessful() {
        when(userQuizRepository.findByUsername(userQuiz_username)).thenReturn(userQuiz);
        when(bcryptEncoder.encode(userQuiz_pass)).thenReturn(userQuiz_pass_enc);

        UserDetails result = jwtUserDetailsService.loadUserByUsername(userQuiz.getUsername());

        Assertions.assertEquals(result.getUsername(), userQuiz_username);
        Assertions.assertEquals(result.getPassword(), userQuiz_pass);
        System.out.println(userQuiz.getUsername());
    }

    @Test
    public void testLoadByNonExistentUsernameFails() {
        lenient().when(userQuizRepository.findByUsername(wrongUsername)).thenReturn(null);

        Assertions.assertThrows(UsernameNotFoundException.class
                , () -> {
                    jwtUserDetailsService.loadUserByUsername(wrongUsername);
                });
    }

}



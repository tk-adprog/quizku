package com.tk.quizku.auth.config;
import com.tk.quizku.app.model.UserQuiz;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JwtTokenUtilTest {
    @Spy
    @InjectMocks
    private JwtTokenUtil jwtTokenUtil;

    private final String userQuiz_username = "ian";
    private final String userQuiz_pass = "password";

    private UserQuiz userQuiz;
    private UserDetails suatuUser;
    private UserDetails wrongUser;

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(jwtTokenUtil, "secret", "ADPRO-TK");

        UserQuiz userQuiz = new UserQuiz();
        userQuiz.setUsername(userQuiz_username);
        userQuiz.setPassword(userQuiz_pass);


        suatuUser = new User(userQuiz_username,userQuiz_pass,new LinkedList<>());
        wrongUser = new User("1906293322", "password000", new LinkedList<>());
    }

    @Test
    public void testGenerateTokenAndValidateShouldSuccess() {
        String token = jwtTokenUtil.generateToken(suatuUser);
        Assertions.assertTrue(jwtTokenUtil.validateToken(token, suatuUser));
    }

    @Test
    public void testGenerateTokenAndValidateDiffUserShouldFail() {
        String token = jwtTokenUtil.generateToken(suatuUser);
        Assertions.assertFalse(jwtTokenUtil.validateToken(token, wrongUser));
    }

    @Test
    public void testExpiredTokenShouldFail() throws ParseException {
        String token = jwtTokenUtil.generateToken(suatuUser);

        when(jwtTokenUtil.isTokenExpired(token)).thenReturn(true);
        Assertions.assertFalse(jwtTokenUtil.validateToken(token, suatuUser));
    }

}

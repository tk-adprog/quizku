package com.tk.quizku.auth.config;

import com.tk.quizku.app.model.UserQuiz;
import com.tk.quizku.auth.service.JwtUserDetailsService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.LinkedList;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JwtRequestFilterTest {
    @InjectMocks
    JwtRequestFilter jwtRequestFilter;

    @Mock
    private JwtUserDetailsService jwtUserDetailsService;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @Mock
    FilterChain filterChain;

    private UserDetails userDetails;
    private UserQuiz userQuiz;
    private final String userQuiz_username = "ian";
    private final String userQuiz_pass = "password";
    private final String jwt_token = "ABCDEFG";
    private MockHttpServletRequest request;

    @BeforeEach
    public void setUp() {
        userQuiz = new UserQuiz();
        userQuiz.setUsername(userQuiz_username);
        userQuiz.setPassword(userQuiz_pass);

        request = new MockHttpServletRequest();
        userDetails = new User(userQuiz_username,userQuiz_pass,new LinkedList<>());

        request.addHeader("Authorization", "Bearer " + jwt_token);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test
    public void testJwtRequestFilterWithCorrectJwtTokenShouldSucceed() throws IOException, ServletException {
        when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenReturn(userQuiz_username);
        when(jwtUserDetailsService.loadUserByUsername(userQuiz_username)).thenReturn(userDetails);
        when(jwtTokenUtil.validateToken(jwt_token, userDetails)).thenReturn(true);

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
    }

    @Test
    public void testJwtRequestFilterWithCorrectJwtTokenShouldSucceedUsingSession() throws IOException, ServletException {
        lenient().when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenReturn(userQuiz_username);
        lenient().when(jwtUserDetailsService.loadUserByUsername(userQuiz_username)).thenReturn(userDetails);
        lenient().when(jwtTokenUtil.validateToken(jwt_token, userDetails)).thenReturn(true);

        request.getSession().setAttribute("Authorization", jwt_token);

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
    }

    @Test
    public void testJwtRequestFilterWithIllegalArgumentShouldFail() throws IOException, ServletException {
        when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenThrow(new IllegalArgumentException());

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(jwtUserDetailsService, times(0)).loadUserByUsername(userQuiz_username);

    }

    @Test
    public void testJwtRequestFilterWithExpiredTokenShouldFail() throws IOException, ServletException {
        when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenThrow(new ExpiredJwtException(null, null, null));

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(jwtUserDetailsService, times(0)).loadUserByUsername(userQuiz_username);
    }

    @Test
    public void testJwtRequestFilterWithNoAuthHeaderShouldFail() throws IOException, ServletException {
        request = new MockHttpServletRequest();

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(jwtUserDetailsService, times(0)).loadUserByUsername(userQuiz_username);
    }

    @Test
    public void testJwtRequestFilterInvalidTokenShouldFail() throws IOException, ServletException {
        when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenReturn(userQuiz_username);
        when(jwtUserDetailsService.loadUserByUsername(userQuiz_username)).thenReturn(userDetails);
        when(jwtTokenUtil.validateToken(jwt_token, userDetails)).thenReturn(false);
        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
    }

    @Test
    public void testJwtRequestFilterHeaderDoesntStartWithBearerShouldFail() throws IOException, ServletException {
        request = new MockHttpServletRequest();
        request.addHeader("Authorization", "NOT BEARER");

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());
        verify(jwtUserDetailsService, times(0)).loadUserByUsername(userQuiz_username);
    }

    @Test
    public void testJwtRequestFilterWithExistingAuthenticationShouldFail() throws IOException, ServletException {
        when(jwtTokenUtil.getUsernameFromToken(jwt_token)).thenReturn(userQuiz_username);
        SecurityContextHolder.getContext().setAuthentication( new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities()));

        jwtRequestFilter.doFilterInternal(request, new MockHttpServletResponse(), new MockFilterChain());

        verify(jwtUserDetailsService, times(0)).loadUserByUsername(userQuiz_username);
    }


}
